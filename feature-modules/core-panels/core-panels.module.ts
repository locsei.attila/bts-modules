import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SimplePanelComponent } from "./simple-panel/simple-panel.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SimplePanelComponent
  ],
  exports: [
    SimplePanelComponent
  ]
})
export class CorePanelsModule { }
